const app = require('./config/config-server');


//starting the server
app.listen(app.get('port'), () => {
    console.log('server listening on port ', app.get('port'));
});