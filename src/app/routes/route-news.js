const express = require('express');
const router = express.Router();
const dbconnection = require('../../config/db-connection');
const connection = dbconnection();

router.get('/', (req, res) => {
    connection.query('SELECT * FROM news', (err, result) => {
        if(!err) {
            res.render('../views/news/view-news.ejs', {
                news: result
            });
        }
    });
    
});

router.post('/add-new', (req, res) => {
    console.log(req.body);
    const {title, new_body} = req.body;
    connection.query(`INSERT INTO news (title, new_body) VALUES ('${title}', '${new_body}')`, (err, result) => {
        if(!err) {
            res.redirect('/');
        }
    });
});

module.exports = router;